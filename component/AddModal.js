import React, { Component } from 'react';
import {
    AppRegistry, FlatList, Style, Text, View, Image, Alert, Dimensions, TextInput
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import FlatListData from '../data/FlatListData'

var screen = Dimensions.get('window');
export default class AddModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newImageName: '',
            newCreatedDate: ''
        }
    }

    showAddModal = () => {
        this.refs.myModal.open()
    }

    // generatKey = (number) => {
    //     return require('radom-string')({length: number});
    // }

    render() {
        return (
            <Modal
                ref={"myModal"}
                style={{
                    justifyContent: 'center',
                    shadowRadius: 10,
                    width: screen.width - 80,
                    height: 300
                }}
                position='center'
                backdrop={true}
            >
                <Text
                    style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginTop: 40
                    }}>Add New Infomation</Text>

                <TextInput
                    style={{
                        height: 40,
                        borderBottomColor: 'gray',
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 20,
                        marginBottom: 10,
                        borderBottomWidth: 1
                    }}
                    onChangeText={(text) => this.setState({ newImageName: text })}
                    placeholder="Nhap ten anh"
                    value={this.state.newImageName}
                />
                <TextInput
                    style={{
                        height: 40,
                        borderBottomColor: 'gray',
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 20,
                        marginBottom: 10,
                        borderBottomWidth: 1
                    }}
                    onChangeText={(text) => this.setState({ newCreatedDate: text })}
                    placeholder="Nhap ngay tao"
                    value={this.state.newCreatedDate}
                />

                <Button
                    style={{
                        fontSize: 18,
                        color: 'white'
                    }}
                    containerStyle={{
                        padding: 8,
                        marginLeft: 70,
                        marginRight: 70,
                        height: 40,
                        borderRadius: 6,
                        backgroundColor: 'mediumseagreen'
                    }}
                    onPress={() => {
                        if (this.state.newImageName.length == 0 || this.state.newCreatedDate.length == 0) {
                            alert("Cần nhập dữ liệu đầy đủ");
                            return;
                        }
                        const newId = 3;
                        const newImage = {
                            id: newId,
                            name: this.state.newImageName,
                            path: "http://anhanghi-dev.jit.vn/storage/YXBwL2hvdXNlcy8xNjI5L2lPem85Mkt6LmpwZw==",
                            updated_at: this.state.newCreatedDate
                        };
                        FlatListData.push(newImage);
                        this.props.parentFlatList.refreshFlatList(newId);
                        this.refs.myModal.close();
                    }}
                >
                    Save
                </Button>


            </Modal>
        );
    }
}