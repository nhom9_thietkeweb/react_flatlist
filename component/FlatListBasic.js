import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, Image, Alert, TouchableHighlight } from 'react-native';
import flatListData from '../data/FlatListData'
import FlatListItem from './FlatListItem'
import AddModal from './AddModal';

export default class FlatListBasic extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            deleteRowKey: null,
        });
        this.onPressAdd = this.onPressAdd.bind(this);
    }

    refreshFlatList = (deleteKey) => {
        this.setState((prevState) =>{
            return{
                deleteRowKey: deleteKey
            }
        });
    }

    onPressAdd() {
        this.refs.addModal.showAddModal();
    }

    render() {
        return (
            <View style={{ flex: 1, marginTop: 25 }}>
                <View style={{
                    backgroundColor: 'tomato',
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    height: 64}}>
                        <TouchableHighlight
                            style={{marginRight: 10}}
                            underlayColor="tomato"
                            onPress={this.onPressAdd}>
                            <Image
                                style={{width: 35, height: 35}}
                                source={require('../assets/icon_add.png')}/>
                        </TouchableHighlight>

                </View>
                <FlatList
                    data={flatListData}
                    renderItem={({ item, index }) => {
                        return (
                            <FlatListItem item={item} index={index} parentFlatList={this}>

                            </FlatListItem>
                        );
                    }}
                >

                </FlatList>
                <AddModal ref={"addModal"} parentFlatList={this}/>
            </View>
        );
    }
}