import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, Image, Alert } from 'react-native';
import flatListData from '../data/FlatListData'
import Swipeout from 'react-native-swipeout';

export default class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRowKey: null
        };
    }
    render() {
        const swipeSettings = {
            autoClose: true,
            onClose: (secId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null });
                }
            },
            onOpen: (secId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.item.id });
            },
            right: [
                {
                    onPress: () => {
                        const deletingRowKey = this.state.activeRowKey;
                        Alert.alert (
                            'Alert',
                            'Are you sure you want to delete?',
                            [
                                {text: 'No', onPress: () => console.log('Cancel Press'), style: 'cancel'},
                                {text: 'Yes', onPress: () => {
                                    flatListData.splice(this.props.index, 1);
                                    this.props.parentFlatList.refreshFlatList(deletingRowKey)
                                }} 
                            ],
                            { cancelable: true}
                        );
                    },
                    text: 'Delete', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        };
        return (
            <Swipeout {...swipeSettings}>
                <View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        // backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen' : 'tomato'
                        backgroundColor: 'mediumseagreen'
                    }}>
                        <Image
                            source={{ uri: this.props.item.path }}
                            style={{ width: 100, height: 100, margin: 5 }}
                        >

                        </Image>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            marginTop: 15
                        }}>
                            <Text style={styles.flatListItem}>Tên ảnh: {this.props.item.name}</Text>
                            <Text style={styles.flatListItem}>Ngày tạo: {this.props.item.updated_at}</Text>
                        </View>
                    </View>
                    <View style={{
                        height: 1,
                        backgroundColor: 'white'
                    }}></View>
                </View>
            </Swipeout>
        );
    }
}
const styles = StyleSheet.create({
    flatListItem: {
        color: 'white',
        padding: 10,
        fontSize: 16
    }
});