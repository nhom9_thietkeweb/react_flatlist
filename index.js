import { AppRegistry, Platform } from 'react-native';
import FlatListBasic from './component/FlatListBasic';

AppRegistry.registerComponent('flatlist', () => FlatListBasic);

if (Platform.OS === 'web') {
  const rootTag = document.getElementById('root') || document.getElementById('main');
  AppRegistry.runApplication('flatlist', { rootTag });
}
